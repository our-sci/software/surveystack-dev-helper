const { MongoClient, Db, ObjectId } = require("mongodb");


main = async () => {
    const args = process.argv
    if (args.length != 5) {
        console.log("need 3 args <group> <username> <email>")
        return;
    }

    const [, , groupname, username, email] = args;

    console.log("group", groupname);
    console.log("username", username);
    console.log("email", email);



    console.log("args", args);
    const client = await new MongoClient("mongodb://localhost:27017").connect()
    const db = await client.db("surveystack-backup");

    const r = await db.collection("groups").find({ name: groupname }).toArray();
    console.log("group", r);

    if (r.length == 0) {
        console.log("group not found");
        process.exit(1);
    }

    const uid = new ObjectId();
    await db.collection("users").insertOne(
        {
            "_id": uid,
            "email": email,
            "name": username,
            "token": "",
            "password": "aaa",
            "permissions": [],
            "authProviders": [],
            "memberships": []
        }
    )


    await db.collection("memberships").insertOne(
        {
            "_id": new ObjectId(),
            "user": uid,
            "group": r[0]._id,
            "role": "user",
            "meta": {
                "status": "active",
                "dateCreated": new Date(),
                "dateSent": null,
                "dateActivated": new Date(),
                "notes": "",
                "invitationEmail": email,
                "invitationName": username,
                "invitationCode": null
            }
        }
    )

    process.exit(0);
}

main()